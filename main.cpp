#include <QApplication>
#include <QTextCodec>
#include "lens_gui.h"

int main(int argc, char* argv[]) 
{
    // ������� ����������
    QApplication app(argc, argv);
    // ������ ���������, ����� ����� ���� ������������ ������� ����  
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("cp1251"));
    // ������� ���������� ����
    DQtOpticalSystem dialog;
    // ��������� ��������� ����
    dialog.show();
    // ��������� ����������
    return app.exec();
}