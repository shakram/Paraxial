#if !defined SPHERICAL_SURFACE_H
#define SPHERICAL_SURFACE_H 
#include "surface.h"


class SphericalSurface : public Surface
{
private:
	double m_r;
public:
	SphericalSurface(); 
	SphericalSurface(double m_r);
	~SphericalSurface();
	void SetR (double r);
	double GetR();
	void GetTransformMatrix(matrix<double>& transform, double n, double n_);

};

#endif