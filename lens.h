#include "surface.h"
#include "paraxial.h"

class Lens
{
private:
	double m_d, m_n;
	Surface m_surface1;
	Surface m_surface2;
public:
	Lens();
	Lens(double d, double n);
	~Lens();
	Surface& AccessSurface1();
	const Surface GetSurface1();
	Surface& AccessSurface2();
	const Surface GetSurface2();
	void SetN (double n);
	double GetN();
	void SetThickness (double d);
	double GetThickness();
	void CalculateParaxial (Paraxial& outparaxial);
};