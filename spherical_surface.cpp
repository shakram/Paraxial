#include "spherical_surface.h"
#include "surface.h"

SphericalSurface::SphericalSurface() 
	: Surface()
{
	m_r = 0;
}

SphericalSurface::SphericalSurface(double r) 
	: Surface()
{
	m_r = r;
}

SphericalSurface::~SphericalSurface()
{
}

void SphericalSurface::SetR (double r)
{
	m_r = r;
}
double SphericalSurface::GetR()
{
	return m_r;
}
void SphericalSurface::GetTransformMatrix ( matrix<double>& transform, double n, double n_)
{
	if (!m_r)
		Surface::GetTransformMatrix(transform, n, n_);
	else
	{
		transform(0, 0) = 1.;
		transform(0, 1) = 0.;
		transform(1, 0) = (- (1 / m_r)) * (n_ - n);
		transform(1, 1) = 1.;
	}
}