#include "surface.h"	

Surface::Surface()
{
	m_h = 0;
}
Surface::Surface(double h)
{
	m_h = h;
}
Surface::~Surface()
{
}
void Surface::SetH(double h)
{
	m_h = h;
}
double Surface::GetH()
{
	return m_h;
}
void Surface::GetTransformMatrix ( matrix<double>& transform, double n, double n_)
{
	transform(0, 0) = 1.;
	transform(0, 1) = 0.;
	transform(1, 0) = 0.;
	transform(1, 1) = 1.;
}
