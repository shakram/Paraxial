#include "lens.h"

Lens::Lens()
{
	m_d = 0;
	m_n = 0;
}

Lens::Lens(double d, double n, double m_r1, double m_r2)
{
	m_d = d;
	m_n = n;
	m_surface1 = SphericalSurface(m_r1);
	m_surface2 = SphericalSurface(m_r2);
}
Lens::~Lens()
{}

Surface& Lens::AccessSurface1()
{
	return m_surface1;
}
const Surface Lens::GetSurface1()
{
	return m_surface1;
}
Surface& Lens::AccessSurface2()
{
	return m_surface2;
}
const Surface Lens::GetSurface2()
{
	return m_surface2;
}
void Lens::SetN (double n)
{
	m_n = n;
}
double Lens::GetN()
{
	return m_n;
}
void Lens::SetThickness (double d)
{
	m_d = d;
}
double Lens::GetThickness()
{
	return m_d;
}
void Lens::CalculateParaxial(Paraxial& outparaxial)
{
	matrix<double> R1(2, 2), R2(2, 2), T(2, 2), G(2, 2);
	m_surface1.GetTransformMatrix(R1, 1, m_n);
	m_surface2.GetTransformMatrix(R2, m_n, 1);
	T(0,0)=1.;
    T(0,1)=m_d / m_n;
    T(1,0)=0;
    T(1,1)=1.;
	G = prod(R1, T);
	G = prod(G, R2);
	outparaxial.m_F = (G(0, 0)*G(1, 1) / G(1, 0)) - G(0, 1);
	outparaxial.m_F_ = -(1 / G(1, 0));
	outparaxial.m_SF = G(1, 1) / G(1, 0);
	outparaxial.m_SF_ = -(G(0, 0) / (G(1, 0)));
	outparaxial.m_SH = outparaxial.m_SF - outparaxial.m_F;
	double SH_ = outparaxial.m_SF_ - outparaxial.m_F_;
}