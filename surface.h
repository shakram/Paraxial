#if !defined SURFACE_H
#define SURFACE_H 
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
using namespace boost::numeric::ublas;

class Surface
{
protected:
		double m_h;
public:
		Surface();
		Surface(double h);
		~Surface();
		void SetH (double h);
		double GetH();
		void GetTransformMatrix ( matrix<double>& transform, double n, double n_);
};

#endif //defined MIRROR_H