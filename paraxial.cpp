#include "paraxial.h"
Paraxial::Paraxial()

{
	m_F = 0;
	m_F_ = 0;
	m_SF = 0;
	m_SF_ = 0;
	m_SH = 0;
	m_SH_ = 0;
}
Paraxial::Paraxial(double F, double F_, double SF, double SF_, double SH, double SH_)
{
	m_F = F;
	m_F_ = F_;
	m_SF = SF;
	m_SF_ = m_SF_;
	m_SH = m_SH;
	m_SH_ = m_SH_;
}
Paraxial::~Paraxial()
{
}