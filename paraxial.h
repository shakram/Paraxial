#if !defined PARAXIAL_H
#define PARAXIAL_H 

class Paraxial
{
public:
	double m_F, m_F_, m_SF, m_SF_, m_SH, m_SH_;
	Paraxial();
	Paraxial(double F, double F_, double SF, double SF_, double SH, double SH_);
	~Paraxial();
};

#endif